package org.kftech.prolo.protoc;

import com.google.protobuf.CodedInputStream;
import google.protobuf.compiler.Plugin;
import org.kftech.prolo.codegen.Generator;
import org.kftech.prolo.codegen.GeneratorResult;

import java.io.IOException;

/**
 * Main class for {@code protoc} plugin that generates {@code prolo} classes.
 */
public class ProtocPlugin {

    public Plugin.CodeGeneratorResponse generateCode(Plugin.CodeGeneratorRequest pluginRequest) {

        Plugin.CodeGeneratorResponse.Builder responseBuilder = Plugin.CodeGeneratorResponse.newBuilder();

        try {
            ProtocPluginGeneratorRequest request = new ProtocPluginGeneratorRequest(pluginRequest);
            Generator generator = new Generator();
            GeneratorResult result = generator.generateCode(request);
            for (GeneratorResult.GeneratedFile file : result.getGeneratedFiles()) {
                Plugin.CodeGeneratorResponse.File responseFile = Plugin.CodeGeneratorResponse.File.newBuilder()
                        .setName(file.getName())
                        .setContent(file.getContent()).build();
                responseBuilder.addFile(responseFile);
            }
        } catch (Exception e) {
            responseBuilder.setError(e.getMessage());
        }
        return responseBuilder.build();
    }

    public static void main(String[] args) {
        try {
            CodedInputStream in = CodedInputStream.newInstance(System.in);
            Plugin.CodeGeneratorRequest pluginRequest = Plugin.CodeGeneratorRequest.newBuilder().mergeFrom(in).build();
            ProtocPlugin plugin = new ProtocPlugin();
            Plugin.CodeGeneratorResponse pluginResponse = plugin.generateCode(pluginRequest);
            pluginResponse.writeTo(System.out);
        } catch (Exception e) {
            Plugin.CodeGeneratorResponse errorResponse = Plugin.CodeGeneratorResponse.newBuilder()
                    .setError(e.getMessage()).build();
            try {
                errorResponse.writeTo(System.out);
            } catch (IOException ioe) {
                // nothing we can do here
            }
        }

    }
}
