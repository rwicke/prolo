package org.kftech.prolo.protoc;

import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.Descriptors;
import google.protobuf.compiler.Plugin;
import org.kftech.prolo.codegen.AbstractGeneratorRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Code generator front-end for use as {@code protoc} plugin.
 */
public class ProtocPluginGeneratorRequest extends AbstractGeneratorRequest {
    protected ProtocPluginGeneratorRequest(Plugin.CodeGeneratorRequest request) throws Exception {
        super(request.getFileToGenerateList());
        buildFileDescriptors(request.getProtoFileList());
    }

    private void buildFileDescriptors(List<DescriptorProtos.FileDescriptorProto> protoFiles) throws Exception {

        Map<String, Descriptors.FileDescriptor> knownDescriptors = new HashMap<String, Descriptors.FileDescriptor>();

        // Relies on the fact that the FileDescriptorProto instances are returned in topological
        // sort order.

        for (DescriptorProtos.FileDescriptorProto protoFile : protoFiles) {
            Descriptors.FileDescriptor[] dependencies = new Descriptors.FileDescriptor[protoFile.getDependencyCount()];
            for (int i = 0; i < protoFile.getDependencyCount(); i++) {
                dependencies[i] = knownDescriptors.get(protoFile.getDependency(i));
            }
            Descriptors.FileDescriptor fileDescriptor = Descriptors.FileDescriptor.buildFrom(protoFile, dependencies);
            knownDescriptors.put(fileDescriptor.getName(), fileDescriptor);
            registerFileDescriptor(fileDescriptor);
        }


    }
}
