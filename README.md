## Prolo - `protobuf` classes with low allocation

### Overview

Prolo provides an alternative code generator for Google Protocol Buffers. The generated Java classes
are intended to have a low allocation profile after initialisation. Message instances are
reusable and in most cases there is no allocation at all. The code generator supports all data types
and also extensions. It does not supports groups, which are a legacy feature anyway.

This is very special-purpose library and if you're wondering whether you going to need it,
you probably don't. I recommend that you stick with the excellent [protobuf](http://code.google.com/p/protobuf/)
implementation from Google or maybe check out [protostuff](http://code.google.com/p/protostuff/)
as an alternative. Prolo is really only useful if you're looking to optimise the overall
allocation profile of your application, e.g. if you're writing a feed handler for market data.

The Prolo code generator currently has a dependency on Google's `protobuf` library as it uses
the descriptor metadata from its generated classes. The generated Prolo classes themselves do
not have any `protobuf` dependencies, however.

The Prolo project consists of three modules:

* `prolo-core` - Core classes referenced by generated code
* `prolo-codegen` - Code generator for Prolo classes (includes a command-line tool)
* `prolo-protoc-plugin` - Prolo code generator as a custom `protoc` plugin.


Note: Some classes in `prolo-core` are derived from classes in Google's protobuf distribution.
See class comments for details.