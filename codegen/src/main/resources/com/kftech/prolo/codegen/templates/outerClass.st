outerClass(fd) ::= <<

package <fd.javaPackageName>;

public final class <fd.javaOuterClassName> {

    <fd.enumTypes:enumClass()>

    <fd.messageTypes:message()>

    <fd.extensionDescriptors:extension()>

}
>>