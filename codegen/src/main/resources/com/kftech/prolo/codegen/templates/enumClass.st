enumClass(enumType) ::= <<
public enum <enumType.name> implements <enumBaseClass()> {

    <enumType.values:{v |<v.name>(<i>, <v.number>)}; separator=",\n">;

    <enumType.values:{v |public static final int <v.name>_VALUE = <v.number>;}; separator="\n">

    public static <enumType.name> valueOf(int value) {
        switch (value) {
            <enumType.values:{v |case <v.number>: return <v.name>;}; separator="\n">
            default: return null;
        }
    }

    public final int getNumber() { return value; }

    private final int index;
    private final int value;

    private <enumType.name>(int index, int value) {
        this.index = index;
        this.value = value;
    }
}
>>

