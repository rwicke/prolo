package org.kftech.prolo.codegen;

import com.google.protobuf.Descriptors;

import java.util.*;

/**
 * Wraps extensions defined in a {@link com.google.protobuf.Descriptors.FileDescriptor} and provides properties
 * similar to those in {@link Descriptors.Descriptor}. This is intended to be used from String Template.
 */
public class ExtensionDescriptor {
    private final Descriptors.Descriptor extendedMessage;
    private final String scope;
    private final List<Descriptors.FieldDescriptor> fields = new ArrayList<Descriptors.FieldDescriptor>();

    public static List<ExtensionDescriptor> getExtensionDescriptors(Descriptors.FileDescriptor file) {
        return getExtensionDescriptors(Utils.getFullyQualifiedOuterClassName(file), file.getExtensions());
    }

    public static List<ExtensionDescriptor> getExtensionDescriptors(Descriptors.Descriptor message) {
        return getExtensionDescriptors(Utils.getFullyQualifiedMessageName(message), message.getExtensions());
    }

    private static List<ExtensionDescriptor> getExtensionDescriptors(String scope, List<Descriptors.FieldDescriptor> fields) {
        List<Descriptors.FieldDescriptor> sortedFields = new ArrayList<Descriptors.FieldDescriptor>(fields);
        Collections.sort(sortedFields, new Comparator<Descriptors.FieldDescriptor>() {
            @Override
            public int compare(Descriptors.FieldDescriptor fd1, Descriptors.FieldDescriptor fd2) {
                return extendedTypeName(fd1).compareTo(extendedTypeName(fd2));
            }
        });

        String currentTypeName = null;
        List<ExtensionDescriptor> extensionDescriptors = new ArrayList<ExtensionDescriptor>();
        List<Descriptors.FieldDescriptor> extensionFields = new ArrayList<Descriptors.FieldDescriptor>();

        for (Iterator<Descriptors.FieldDescriptor> iter = sortedFields.iterator(); iter.hasNext(); ) {
            final Descriptors.FieldDescriptor field = iter.next();
            final String typeName = extendedTypeName(field);
            if (currentTypeName == null) {
                currentTypeName = typeName;
            }

            if (typeName.equals(currentTypeName)) {
                extensionFields.add(field);
            } else {
                extensionDescriptors.add(new ExtensionDescriptor(scope, extensionFields));
                extensionFields.clear();
                currentTypeName = typeName;
            }
        }

        if (extensionFields.size() > 0) {
            extensionDescriptors.add(new ExtensionDescriptor(scope, extensionFields));
        }

        return extensionDescriptors;
    }

    private static String extendedTypeName(Descriptors.FieldDescriptor field) {
        return Utils.getFullyQualifiedMessageName(field.getContainingType());
    }


    public ExtensionDescriptor(String scope, Collection<Descriptors.FieldDescriptor> fields) {
        assert fields.size() > 0;
        this.scope = scope;
        this.fields.addAll(fields);
        this.extendedMessage = this.fields.get(0).getContainingType();
    }

    public String getName() {
        return extendedMessage.getName() + "Extension";
    }

    public String getFqName() {
        return scope + "." + extendedMessage.getName() + "Extension";
    }

    public Descriptors.Descriptor getExtendedType() {
        return extendedMessage;
    }

    public List<Descriptors.FieldDescriptor> getMessageFields() {
        ArrayList<Descriptors.FieldDescriptor> messageFields = new ArrayList<Descriptors.FieldDescriptor>();
        for (Descriptors.FieldDescriptor field : fields) {
            if (field.getType() == Descriptors.FieldDescriptor.Type.MESSAGE) {
                messageFields.add(field);
            }
        }
        return messageFields;
    }

    public List<Descriptors.Descriptor> getReferencedMessageTypes() {
        return Utils.getReferencedMessageTypes(fields);
    }

    public boolean isExtensionType() {
        return true;
    }

    public List<Descriptors.FieldDescriptor> getFields() {
        return fields;
    }

}
