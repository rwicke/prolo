package org.kftech.prolo.codegen;

import com.google.protobuf.Descriptors;

import java.util.List;

/**
 * Holds the input to the {@code prolo} code generator.
 */
public interface GeneratorRequest {

    /**
     * Protobuf files to generate. This are expected to be {@code .proto} paths as returned by
     * {@link com.google.protobuf.Descriptors.FileDescriptor#getName()} .
     * @return
     */
    List<String> getFilesToGenerate();

    /**
     * Returns the file descriptor for the given protobuf file name. This is expected to be one
     * of the file names returned by {@link #getFilesToGenerate()}.
     * @param fileName protobuf file name
     * @return file descriptor or {@code null} if no file descriptor of the given name is known
     */
    Descriptors.FileDescriptor getFileDescriptor(String fileName);
}
