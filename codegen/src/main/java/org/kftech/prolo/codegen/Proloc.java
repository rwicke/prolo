package org.kftech.prolo.codegen;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Code generator command-line tool.
 */
public class Proloc {

    private static final List<String> ALL_FILES_WILDCARD = Collections.emptyList();

    @Option(name = "-o", usage = "output directory", metaVar = "DIR")
    public File outputDirectory;

    @Option(name= "-f", usage = "file descriptor set", metaVar = "FILE")
    public File descriptorSetFile;

    @Argument
    public List<String> arguments = new ArrayList<String>();

    public void doMain(String[] args) {
        final CmdLineParser parser = new CmdLineParser(this);
        parser.setUsageWidth(80);

        try {
            parser.parseArgument(args);
            checkArguments(parser);

            if (!outputDirectory.mkdirs()) {
                fail(parser, "could not create output directory '" + outputDirectory + "'");
            }

            // if we get here, arguments look OK

            try {
                final GeneratorRequest request = createGeneratorRequest();
                final GeneratorResult result = generateCode(request);
                writeResult(result);
            } catch (Exception e) {
                e.printStackTrace();
                fail(parser, e.getMessage());
            }
        } catch (CmdLineException e) {
            fail(parser, e.getMessage());
        }
    }

    private GeneratorResult generateCode(final GeneratorRequest request) throws Exception {
        final Generator generator = new Generator();
        return generator.generateCode(request);
    }

    private void writeResult(final GeneratorResult result) throws IOException {
        for (GeneratorResult.GeneratedFile generatedFile : result.getGeneratedFiles()) {
            writeFile(generatedFile);
        }
    }

    private void writeFile(final GeneratorResult.GeneratedFile generatedFile) throws IOException {
        PrintWriter out = null;
        try {
            final int pos = generatedFile.getName().lastIndexOf('/');
            final File packagePath;
            if (pos != -1) {
                packagePath = new File(outputDirectory, generatedFile.getName().substring(0, pos));
            } else {
                packagePath = outputDirectory;
            }
            packagePath.mkdirs();
            final File outFile = new File(packagePath, generatedFile.getName().substring(pos + 1));
            out = new PrintWriter(new FileWriter(outFile));
            out.println(generatedFile.getContent());
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    private void checkArguments(CmdLineParser parser) {
        if (outputDirectory == null) {
            fail(parser, "must specify output directory");
        }

        if (descriptorSetFile != null) {
            if (!descriptorSetFile.isFile()) {
                fail(parser, "descriptor set file '" + descriptorSetFile + "' does not exist");
            }

            if (arguments.isEmpty()) {
                fail(parser, "must specify at least one proto file");
            }
        } else {
            if (arguments.isEmpty()) {
                fail(parser, "must specify the name of a protoc-generated class");
            }
            for (String className : arguments) {
                try {
                    Class.forName(className);
                } catch (Exception e) {
                    fail(parser, "could not load class '" + className + "'");
                }
            }
        }
    }

    public static void main(String[] args) {
        new Proloc().doMain(args);
    }

    private GeneratorRequest createGeneratorRequest() {
        if (descriptorSetFile != null) {
            return new FileDescriptorSetGeneratorRequest(arguments, descriptorSetFile);
        } else {
            return new ProtoClassGeneratorRequest(ALL_FILES_WILDCARD, arguments);
        }
    }


    private static void fail(CmdLineParser parser, String message) {
        new Exception().printStackTrace();
        System.err.println(message);
        System.err.println();
        System.err.println("Usages:");
        System.err.println("  java " + Proloc.class.getName() + " -o <outdir> -f <descrfile> <proto1> <proto2> ...");
        System.err.println("  java " + Proloc.class.getName() + " -o <outdir> <protoclass1> <protoclass2> ...");
        System.err.println();
        System.err.println("Arguments:");
        parser.printUsage(System.err);
        System.err.println();
        System.exit(-1);
    }


}
