package org.kftech.prolo.codegen;

import org.stringtemplate.v4.Interpreter;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.misc.ObjectModelAdaptor;
import org.stringtemplate.v4.misc.STNoSuchPropertyException;

import java.util.HashMap;
import java.util.Map;

/**
 * Base class for extending object models with custom properties.
 */
public abstract class ObjectExtensionAdaptor<T> extends ObjectModelAdaptor {

    public interface ExtensionFunction<T> {

        Object call(Interpreter interp, ST self, T o, String propertyName);

    }

    private final Map<String, ExtensionFunction<T>> extensions = new HashMap<String, ExtensionFunction<T>>();

    @Override
    public Object getProperty(Interpreter interp, ST self, Object o, Object property, String propertyName) throws STNoSuchPropertyException {
        final ExtensionFunction<T> extension = lookupExtension(propertyName);

        if (extension != null) {
            //noinspection unchecked
            return extension.call(interp, self, (T)o, propertyName);
        } else {
            return super.getProperty(interp, self, o, property, propertyName);
        }
    }

    protected void addExtension(String propertyName, ExtensionFunction<T> extension) {
        extensions.put(propertyName, extension);
    }

    protected ExtensionFunction<T> lookupExtension(String propertyName) {
        return extensions.get(propertyName);
    }


}
