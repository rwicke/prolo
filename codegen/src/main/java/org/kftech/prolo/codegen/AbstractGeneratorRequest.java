package org.kftech.prolo.codegen;

import com.google.protobuf.Descriptors;

import java.util.*;

/**
 * Abstract base class for generator request implementations.
 */
public abstract class AbstractGeneratorRequest implements GeneratorRequest {

    private final List<String> filesToGenerate;
    private final Map<String, Descriptors.FileDescriptor> fileDescriptors =
            new HashMap<String, Descriptors.FileDescriptor>();

    protected AbstractGeneratorRequest(List<String> filesToGenerate) {
        this.filesToGenerate = Collections.unmodifiableList(new ArrayList<String>(filesToGenerate));
    }

    @Override
    public List<String> getFilesToGenerate() {
        return filesToGenerate;
    }

    @Override
    public Descriptors.FileDescriptor getFileDescriptor(String fileName) {
        return fileDescriptors.get(fileName);
    }

    protected void registerFileDescriptor(Descriptors.FileDescriptor fileDescriptor) {
        fileDescriptors.put(fileDescriptor.getName(), fileDescriptor);
    }

    protected Map<String, Descriptors.FileDescriptor> getAllFileDescriptors() {
        return Collections.unmodifiableMap(fileDescriptors);
    }
}
