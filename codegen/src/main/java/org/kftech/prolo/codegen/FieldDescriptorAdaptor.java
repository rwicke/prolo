package org.kftech.prolo.codegen;

import com.google.protobuf.ByteString;
import com.google.protobuf.Descriptors;
import org.kftech.prolo.MutableAsciiString;
import org.kftech.prolo.MutableByteString;
import org.kftech.prolo.ProtobufEncoder;
import org.kftech.prolo.WireFormatHelper;
import org.kftech.prolo.collections.*;
import org.stringtemplate.v4.Interpreter;
import org.stringtemplate.v4.ST;


/**
 * Extends {@link Descriptors.FieldDescriptor} with additional properties for use in String Template.
 */
public class FieldDescriptorAdaptor extends ObjectExtensionAdaptor<Descriptors.FieldDescriptor> {

    public FieldDescriptorAdaptor() {
        addExtension("fieldBaseName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getFieldBaseName(o);
            }
        });
        addExtension("fieldNumberConstantName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getFieldNumberConstantName(o);
            }
        });
        addExtension("fieldDefaultConstantName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getFieldDefaultValueConstantName(o);
            }
        });
        addExtension("fieldName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getFieldName(o);
            }
        });
        addExtension("hasFieldName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getHasFieldName(o);
            }
        });
        addExtension("hasDefaultValue", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return o.hasDefaultValue();
            }
        });
        addExtension("setterName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getSetterName(o);
            }
        });
        addExtension("getterName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getGetMethodName(o);
            }
        });
        addExtension("messageCreatorName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getMessageCreatorName(o);
            }
        });
        addExtension("countMethodName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getCountMethodName(o);
            }
        });
        addExtension("hasMethodName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getHasMethodName(o);
            }
        });
        addExtension("ensureMethodName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return "ensure" + Utils.toCamelCase(o, true);
            }
        });
        addExtension("clearMethodName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getClearMethodName(o);
            }
        });
        addExtension("addMethodName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getAddMethodName(o);
            }
        });
        addExtension("javaClass", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                switch (o.getJavaType()) {
                    case BOOLEAN:
                        return "boolean";
                    case INT:
                        return "int";
                    case LONG:
                        return "long";
                    case DOUBLE:
                        return "double";
                    case FLOAT:
                        return "float";
                    case ENUM:
                        return o.getEnumType().getName();
                    case MESSAGE:
                        return Utils.getFullyQualifiedMessageName(o.getMessageType());
                    case STRING:
                        return MutableAsciiString.class.getName();
                    case BYTE_STRING:
                        return MutableByteString.class.getName();
                    default:
                        throw new AssertionError("unexpected type: " + o.getJavaType());
                }

            }
        });
        addExtension("javaListClass", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                switch (o.getJavaType()) {
                    case BOOLEAN:
                        return BooleanList.class.getName();
                    case INT:
                        return IntList.class.getName();
                    case LONG:
                        return LongList.class.getName();
                    case DOUBLE:
                        return DoubleList.class.getName();
                    case FLOAT:
                        return FloatList.class.getName();
                    case ENUM:
                        return "java.util.ArrayList<" + o.getEnumType().getName() + ">";
                    case MESSAGE:
                        final Descriptors.Descriptor messageType = o.getMessageType();
                        final String fqMessageName = Utils.getFullyQualifiedMessageName(messageType);
                        return MessageList.class.getName() + "<" + fqMessageName + ">";
                    case STRING:
                        return MutableStringList.class.getName();
                    case BYTE_STRING:
                        return MutableByteStringList.class.getName();
                    default:
                        throw new AssertionError("unexpected type: " + o.getJavaType());
                }
            }
        });
        addExtension("fieldCategory", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                Descriptors.FieldDescriptor.JavaType javaType = o.getJavaType();
                if (o.isRepeated()) {
                    switch (javaType) {
                        case BYTE_STRING:
                            return "repeatedBytesField";
                        case STRING:
                            return "repeatedStringField";

                        case MESSAGE:
                            return "repeatedMessageField";

                        default:
                            return "repeatedScalarField";
                    }
                } else {
                    switch (javaType) {
                        case BYTE_STRING:
                            return "bytesField";
                        case STRING:
                            return "stringField";

                        case MESSAGE:
                            return "messageField";

                        default:
                            return "scalarField";
                    }
                }
            }
        });
        addExtension("defaultValueLiteral", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor field, String propertyName) {
                Descriptors.FieldDescriptor.JavaType javaType = field.getJavaType();
                switch (javaType) {
                    case BOOLEAN:
                    case INT:
                        return field.getDefaultValue();

                    case LONG:
                        return field.getDefaultValue() + "L";

                    case ENUM:
                        String enumType = field.getEnumType().getName();
                        String enumValue = ((Descriptors.EnumValueDescriptor) field.getDefaultValue()).getName();
                        return enumType + "." + enumValue;

                    case DOUBLE:
                        Double d = (Double) field.getDefaultValue();
                        if (d.isNaN()) {
                            return "java.lang.Double.NaN";
                        } else if (d.isInfinite()) {
                            if (d < 0.0d) {
                                return "java.lang.Double.NEGATIVE_INFINITY";
                            } else {
                                return "java.lang.Double.POSITIVE_INFINITY";
                            }
                        } else {
                            if (field.hasDefaultValue()) {
                                // return unparsed double value
                                return field.toProto().getDefaultValue() + "d";
                            } else {
                                return "0.0d";
                            }
                        }

                    case FLOAT:
                        Float f = (Float) field.getDefaultValue();
                        if (f.isNaN()) {
                            return "java.lang.Float.NaN";
                        } else if (f.isInfinite()) {
                            if (f < 0.0f) {
                                return "java.lang.Float.NEGATIVE_INFINITY";
                            } else {
                                return "java.lang.Float.POSITIVE_INFINITY";
                            }
                        } else {
                            if (field.hasDefaultValue()) {
                                // return unparsed float value
                                return field.toProto().getDefaultValue() + "f";
                            } else {
                                return "0.0f";
                            }
                        }

                    case STRING:
                        return "\"" + field.getDefaultValue() + "\"";

                    case BYTE_STRING:
                        StringBuilder sb = new StringBuilder();
                        sb.append("{");
                        ByteString bs = (ByteString) field.getDefaultValue();
                        for (int i = 0; i < bs.size(); i++) {
                            if (i > 0) {
                                sb.append(", ");
                            }
                            sb.append(bs.byteAt(i));
                        }
                        sb.append("}");
                        return sb.toString();

                    default:
                        throw new AssertionError("field type not a scalar: " + field.getType().name());
                }
            }
        });
        addExtension("fieldTag", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getFieldTag(o, false);
            }
        });
        addExtension("fieldTagPacked", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getFieldTag(o, true);
            }
        });
        addExtension("encoderWriteMethodName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getEncoderWriteMethodName(o);
            }
        });
        addExtension("encoderWriteNoTagMethodName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getEncoderWriteNoTagMethodName(o);
            }
        });
        addExtension("encoderComputeMethodName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getEncoderComputeMethodName(o);
            }
        });
        addExtension("encoderComputeNoTagMethodName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getEncoderComputeNoTagMethodName(o);
            }
        });
        addExtension("encoderComputeTagSizeMethodName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return ProtobufEncoder.class.getName() + ".computeTagSize";
            }
        });
        addExtension("encoderComputeLengthSizeMethodName", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return ProtobufEncoder.class.getName() + ".computeRawVarint32Size";
            }
        });
        addExtension("decoderReadMethod", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return Utils.getDecoderReadMethod(o);
            }
        });
        addExtension("kWireTypeLengthDelimited", new ExtensionFunction<Descriptors.FieldDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.FieldDescriptor o, String propertyName) {
                return WireFormatHelper.WIRETYPE_LENGTH_DELIMITED;
            }
        });
    }

}
