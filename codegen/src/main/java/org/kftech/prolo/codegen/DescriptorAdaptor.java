package org.kftech.prolo.codegen;

import com.google.protobuf.Descriptors;
import org.stringtemplate.v4.Interpreter;
import org.stringtemplate.v4.ST;

import java.util.ArrayList;

/**
 * Extends {@link Descriptors.Descriptor} with additional properties for use in String Template.
 */
public class DescriptorAdaptor extends ObjectExtensionAdaptor<Descriptors.Descriptor> {

    public DescriptorAdaptor() {
        addExtension("fqName", new ExtensionFunction<Descriptors.Descriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.Descriptor o, String propertyName) {
                return Utils.getFullyQualifiedMessageName(o);
            }
        });
        addExtension("referencedMessageTypes", new ExtensionFunction<Descriptors.Descriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.Descriptor o, String propertyName) {
                return Utils.getReferencedMessageTypes(o);
            }
        });
        addExtension("allocatorClassName", new ExtensionFunction<Descriptors.Descriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.Descriptor o, String propertyName) {
                return Utils.getAllocatorClassName(o);
            }
        });
        addExtension("fqAllocatorClassName", new ExtensionFunction<Descriptors.Descriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.Descriptor o, String propertyName) {
                return Utils.getAllocatorClassNameFqn(o);
            }
        });
        addExtension("allocatorFieldName", new ExtensionFunction<Descriptors.Descriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.Descriptor o, String propertyName) {
                return Utils.getAllocatorFieldName(o);
            }
        });
        addExtension("messageFields", new ExtensionFunction<Descriptors.Descriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.Descriptor o, String propertyName) {
                ArrayList<Descriptors.FieldDescriptor> fields = new ArrayList<Descriptors.FieldDescriptor>();
                for (Descriptors.FieldDescriptor field : o.getFields()) {
                    if (field.getType() == Descriptors.FieldDescriptor.Type.MESSAGE) {
                        fields.add(field);
                    }
                }
                return fields;
            }
        });
        addExtension("extensionClassName", new ExtensionFunction<Descriptors.Descriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.Descriptor o, String propertyName) {
                return o.getName() + "Extension";
            }
        });
        addExtension("fqExtensionClassName", new ExtensionFunction<Descriptors.Descriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.Descriptor o, String propertyName) {
                return Utils.getFullyQualifiedMessageName(o) + "Extension";
            }
        });

        addExtension("extensionDescriptors", new ExtensionFunction<Descriptors.Descriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.Descriptor o, String propertyName) {
                return ExtensionDescriptor.getExtensionDescriptors(o);
            }
        });

    }

}
