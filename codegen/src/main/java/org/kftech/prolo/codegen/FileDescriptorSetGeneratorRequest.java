package org.kftech.prolo.codegen;

import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.Descriptors;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A generator request based on a (self-contained) {@link DescriptorProtos.FileDescriptorSet} as output
 * by the {@code protoc} compiler.
 */
public class FileDescriptorSetGeneratorRequest extends AbstractGeneratorRequest {

    /**
     * Creates a new request.
     * @param protoFiles proto file paths to be selected from the file descriptor set
     * @param descriptorSetProto file containing a serialized {@link DescriptorProtos.FileDescriptorSet} instance
     */
    public FileDescriptorSetGeneratorRequest(List<String> protoFiles, File descriptorSetProto) {
        super(protoFiles);
        loadFileDescriptors(descriptorSetProto);
    }

    private void loadFileDescriptors(File descriptorSetProto) {
        InputStream in = null;
        try {
            in = new FileInputStream(descriptorSetProto);
            DescriptorProtos.FileDescriptorSet.Builder builder = DescriptorProtos.FileDescriptorSet.newBuilder();
            DescriptorProtos.FileDescriptorSet fileSetProto = builder.mergeFrom(in).build();

            Map<String, DescriptorProtos.FileDescriptorProto> fileProtos = buildFileDescriptorProtoMap(fileSetProto);
            Map<String, Descriptors.FileDescriptor> files = new HashMap<String, Descriptors.FileDescriptor>();
            for (DescriptorProtos.FileDescriptorProto fileProto : fileSetProto.getFileList()) {
                registerFileDescriptor(buildFileDescriptor(fileProtos, files, fileProto.getName()));
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    // nothing to do
                }
            }
        }
    }

    private static Descriptors.FileDescriptor buildFileDescriptor(
            Map<String, DescriptorProtos.FileDescriptorProto> protos,
            Map<String, Descriptors.FileDescriptor> files,
            String fileName) throws Descriptors.DescriptorValidationException {

        if (fileName == null) {
            return null;
        }

        if (files.containsKey(fileName)) {
            return files.get(fileName);
        }

        DescriptorProtos.FileDescriptorProto proto = protos.get(fileName);
        if (proto == null) {
            throw new IllegalArgumentException("file not found: " + fileName);
        }

        List<Descriptors.FileDescriptor> dependencies = new ArrayList<Descriptors.FileDescriptor>();
        for (String depFileName : proto.getDependencyList()) {
            dependencies.add(buildFileDescriptor(protos, files, depFileName));
        }

        Descriptors.FileDescriptor file = Descriptors.FileDescriptor.buildFrom(proto,
                dependencies.toArray(new Descriptors.FileDescriptor[dependencies.size()]));

        files.put(fileName, file);

        return file;
    }


    private static Map<String, DescriptorProtos.FileDescriptorProto> buildFileDescriptorProtoMap(
            DescriptorProtos.FileDescriptorSet files) {
        HashMap<String, DescriptorProtos.FileDescriptorProto> map = new HashMap<String, DescriptorProtos.FileDescriptorProto>();
        for (int i = 0; i < files.getFileCount(); i++) {
            map.put(files.getFile(i).getName(), files.getFile(i));
        }
        return map;
    }
}
