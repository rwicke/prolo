package org.kftech.prolo.codegen;

import com.google.protobuf.Descriptors;
import org.kftech.prolo.ProtobufEncoder;
import org.kftech.prolo.WireFormatHelper;

import java.util.*;

/**
 * Various utility methods.
 */
public final class Utils {

    private Utils() {

    }

    public static int getWireType(Descriptors.FieldDescriptor fieldType, boolean packed) {
        if (packed && !fieldType.isPackable()) {
            throw new IllegalArgumentException(fieldType.getType().name() + " not packable");
        }

        if (packed) {
            return WireFormatHelper.WIRETYPE_LENGTH_DELIMITED;
        }

        switch (fieldType.getType()) {
        case BOOL:
        case INT32:
        case INT64:
        case UINT32:
        case UINT64:
        case SINT32:
        case SINT64:
        case ENUM:
            return WireFormatHelper.WIRETYPE_VARINT;

        case FIXED32:
        case SFIXED32:
        case FLOAT:
            return WireFormatHelper.WIRETYPE_FIXED32;

        case FIXED64:
        case SFIXED64:
        case DOUBLE:
            return WireFormatHelper.WIRETYPE_FIXED64;

        case BYTES:
        case STRING:
        case MESSAGE:
            return WireFormatHelper.WIRETYPE_LENGTH_DELIMITED;

        default:
            throw new AssertionError("unsuported type: " + fieldType.getType());

        }
    }

    public static int getFieldTag(Descriptors.FieldDescriptor fieldType, boolean packed) {
        return WireFormatHelper.makeTag(fieldType.getNumber(), getWireType(fieldType, packed));
    }

    public static String getTypeString(Descriptors.FieldDescriptor fieldType) {
        switch (fieldType.getType()) {
        case BOOL: return "Bool";
        case DOUBLE: return "Double";
        case FLOAT: return "Float";
        case FIXED32: return "Fixed32";
        case FIXED64: return "Fixed64";
        case INT32: return "Int32";
        case INT64: return "Int64";
        case SFIXED32: return "SFixed32";
        case SFIXED64: return "SFixed64";
        case UINT32: return "UInt32";
        case UINT64: return "UInt64";
        case SINT32: return "SInt32";
        case SINT64 : return "SInt64";
        case MESSAGE : return "Message";
        case ENUM : return "Enum";
        case STRING: return "String";
        case BYTES: return "Bytes";
        default:
            throw new UnsupportedOperationException("unsupported type: " + fieldType.getType());
        }
    }

    public static String getEncoderWriteMethodName(Descriptors.FieldDescriptor fieldType) {
        return "write" + getTypeString(fieldType);
    }

    public static String getEncoderWriteNoTagMethodName(Descriptors.FieldDescriptor fieldType) {
        return getEncoderWriteMethodName(fieldType) + "NoTag";
    }

    public static String getEncoderComputeMethodName(Descriptors.FieldDescriptor fieldType) {
        return ProtobufEncoder.class.getName() + ".compute"
                + getTypeString(fieldType) + "Size";
    }

    public static String getEncoderComputeNoTagMethodName(Descriptors.FieldDescriptor fieldType) {
        return ProtobufEncoder.class.getName() + ".compute"
                + getTypeString(fieldType) + "SizeNoTag";
    }

    public static String getDecoderReadMethod(Descriptors.FieldDescriptor fieldType) {
        return "read" + getTypeString(fieldType);
    }

    /**
     * Returns the Java package name for generated artifacts of the given
     * file descriptor.
     * @param protoFile protobuf file descriptor
     * @return Java package name
     */
    public static String getPackageName(Descriptors.FileDescriptor protoFile) {
        if (protoFile.getOptions().hasJavaPackage()) {
            return protoFile.getOptions().getJavaPackage();
        } else {
            return protoFile.getPackage();
        }
    }

    /**
     * Returns the unqualified outer class name that contains all generated
     * artifacts of the given file descriptor.
     * @param protoFile protobuf file descriptor
     * @return unqualified class name
     */
    public static String getOuterClassName(Descriptors.FileDescriptor protoFile) {
        if (protoFile.getOptions().hasJavaOuterClassname()) {
            return protoFile.getOptions().getJavaOuterClassname() + "LowAlloc";
        } else {
            String protoFileName = protoFile.getName();
            int dotPos = protoFileName.indexOf('.');
            if (dotPos != -1) {
                protoFileName = protoFileName.substring(0, dotPos);
            }
            return toCamelCaseName(protoFileName, true) + "LowAlloc";
        }

    }

    /**
     * Returns the fully-qualified message class name for the given message
     * descriptor.
     * @param messageType message type descriptor
     * @return fully-qualified Java class name
     */
    public static String getFullyQualifiedMessageName(Descriptors.Descriptor messageType) {
        final Descriptors.FileDescriptor file = messageType.getFile();
        final String packageName = getPackageName(file);
        final String baseName = packageName + "." + getOuterClassName(file);

        Descriptors.Descriptor currentMessageType = messageType;
        String classPath = "";
        while (currentMessageType != null) {
            classPath = "." + currentMessageType.getName() + classPath;
            currentMessageType = currentMessageType.getContainingType();
        }
        return baseName + classPath;
    }

    public static String getFullyQualifiedEnumName(Descriptors.EnumDescriptor messageType) {
        final Descriptors.FileDescriptor file = messageType.getFile();
        final String packageName = getPackageName(file);
        final String baseName = packageName + "." + getOuterClassName(file);
        String classPath = "." + messageType.getName();
        Descriptors.Descriptor currentMessageType = messageType.getContainingType();
        while (currentMessageType != null) {
            classPath = "." + currentMessageType.getName() + classPath;
            currentMessageType = currentMessageType.getContainingType();
        }
        return baseName + classPath;
    }

    public static String getFullyQualifiedOuterClassName(Descriptors.FileDescriptor file) {
        return getPackageName(file) + "." + getOuterClassName(file);
    }

    /**
     * Converts the given identifier to a mixed (camel) case name. Leading
     * and trailing underscores are removed. Internal underscores are removed
     * and the following character converted to upper case.
     *
     * @param identifier name to be converted
     * @param firstUpper if <tt>true</tt> the first character will become
     * upper case; otherwise it will become lower case.
     * @return converted identifier
     */
    public static String toCamelCaseName(String identifier, boolean firstUpper) {
        String name = identifier.replaceFirst("^_+", "").replaceFirst("_+$", "");
        final StringBuilder sb = new StringBuilder();
        boolean makeUpper = false;
        boolean first = true;
        for (char ch : name.toCharArray()) {
            if (ch == '_') {
                makeUpper = true;
            } else if (first) {
                if (firstUpper) {
                    sb.append(Character.toUpperCase(ch));
                } else {
                    sb.append(Character.toLowerCase(ch));
                }
                first = false;
            } else if (makeUpper) {
                sb.append(Character.toUpperCase(ch));
                makeUpper = false;
            } else {
                sb.append(ch);
            }
        }

        return sb.toString();
    }

    public static String toCamelCase(Descriptors.FieldDescriptor fieldType, boolean firstUpper) {
        return toCamelCaseName(fieldType.getName(), firstUpper);
    }

    public static String toCamelCase(Descriptors.Descriptor messageType, boolean firstUpper) {
        return toCamelCaseName(messageType.getName(), firstUpper);
    }

    /**
     * Returns the base name for a message field, which is its camel
     * case form with an initial lower case letter
     * @param fieldType message field descriptor
     * @return field base name
     */
    public static String getFieldBaseName(Descriptors.FieldDescriptor fieldType) {
        return toCamelCaseName(fieldType.getName(), false);
    }

    /**
     * Returns the name of the given field's tag constant.
     * @param fieldDescriptor message field descriptor
     * @return tag constant name
     */
    public static String getFieldNumberConstantName(Descriptors.FieldDescriptor fieldDescriptor) {
        String name = fieldDescriptor.getName().replaceFirst("^_+", "").replaceFirst("_+$", "");
        return "FIELD_NUMBER_" + name.toUpperCase();
    }

    public static String getFieldDefaultValueConstantName(Descriptors.FieldDescriptor fieldDescriptor) {
        String name = fieldDescriptor.getName().replaceFirst("^_+", "").replaceFirst("_+$", "");
        return "DEFAULT_VALUE_" + name.toUpperCase();
    }

    /**
     * Returns the Java field name for the given message field. This is the
     * field base name followed by an underscore.
     * @param fieldType message field descriptor
     * @return field name
     */
    public static String getFieldName(Descriptors.FieldDescriptor fieldType) {
        return getFieldBaseName(fieldType) + "_";
    }

    /**
     * Returns the name of the boolean field used to check whether
     * a message field has been set.
     * @param fieldType
     * @return
     */
    public static String getHasFieldName(Descriptors.FieldDescriptor fieldType) {
        return "has" + toCamelCase(fieldType, true) + "_";
    }

    /**
     * Returns the method name for field setters.
     * @param fieldType
     * @return
     */
    public static String getSetterName(Descriptors.FieldDescriptor fieldType) {
        return "set" + toCamelCase(fieldType, true);
    }

    public static String getMessageCreatorName(Descriptors.FieldDescriptor fieldType) {
        return "new" + toCamelCase(fieldType, true);
    }

    public static String getGetMethodName(Descriptors.FieldDescriptor fieldType) {
        return "get" + toCamelCase(fieldType, true);
    }

    public static String getCountMethodName(Descriptors.FieldDescriptor fieldType) {
        return "get" + toCamelCase(fieldType, true) + "Count";
    }

    public static String getHasMethodName(Descriptors.FieldDescriptor fieldType) {
        return "has" + toCamelCase(fieldType, true);
    }

    public static String getClearMethodName(Descriptors.FieldDescriptor fieldType) {
        return "clear" + toCamelCase(fieldType, true);
    }

    public static String getAddMethodName(Descriptors.FieldDescriptor fieldType) {
        return "add" + toCamelCase(fieldType, true);
    }

    public static String toFirstUpperCase(String str) {
        return Character.toUpperCase(str.charAt(0)) + str.substring(1);
    }

    public static String toFirstLowerCase(String str) {
        return Character.toLowerCase(str.charAt(0)) + str.substring(1);
    }

    public static List<Descriptors.Descriptor> getReferencedMessageTypes(Descriptors.Descriptor messageType) {
        return getReferencedMessageTypes(messageType.getFields());
    }

    public static List<Descriptors.Descriptor> getReferencedMessageTypes(Collection<Descriptors.FieldDescriptor> fields) {
        Map<String, Descriptors.Descriptor> referencedTypes = new HashMap<String, Descriptors.Descriptor>();
        for (Descriptors.FieldDescriptor fieldType : fields) {
            if (fieldType.getType() == Descriptors.FieldDescriptor.Type.MESSAGE) {
                Descriptors.Descriptor referencedType = fieldType.getMessageType();
                referencedTypes.put(referencedType.getFullName(), referencedType);
            }
        }

        return new ArrayList<Descriptors.Descriptor>(referencedTypes.values());

    }

    public static String getAllocatorClassName(Descriptors.Descriptor messageType) {
        return messageType.getName() + "Allocator";
    }

    public static String getAllocatorClassNameFqn(Descriptors.Descriptor messageType) {
        return getFullyQualifiedMessageName(messageType) + "Allocator";
    }

    public static String getAllocatorFieldName(Descriptors.Descriptor messageType) {
        return toFirstLowerCase(getAllocatorClassName(messageType));
    }

}
