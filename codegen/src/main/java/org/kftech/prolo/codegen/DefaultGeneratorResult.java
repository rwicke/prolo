package org.kftech.prolo.codegen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Default implementation of {@link GeneratorResult}.
 */
class DefaultGeneratorResult implements GeneratorResult {

    private List<GeneratedFile> generatedFiles = new ArrayList<GeneratedFile>();

    public void addGeneratedFile(String fileName, String content) {
        generatedFiles.add(new DefaultGeneratedFile(fileName, content));
    }

    @Override
    public List<GeneratedFile> getGeneratedFiles() {
        return Collections.unmodifiableList(generatedFiles);
    }

    static class DefaultGeneratedFile implements GeneratedFile {
        private final String name;
        private final String content;

        DefaultGeneratedFile(String name, String content) {
            this.name = name;
            this.content = content;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getContent() {
            return content;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
