package org.kftech.prolo.codegen;

import com.google.protobuf.Descriptors;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;


/**
 * Generates Java source code for {@code prolo} protobuf classes from {@link com.google.protobuf.Descriptors.FileDescriptor}
 * instances.
 */
public class Generator {

    public GeneratorResult generateCode(GeneratorRequest request) throws Exception {
        DefaultGeneratorResult result = new DefaultGeneratorResult();
        for (String fileName : request.getFilesToGenerate()) {
            Descriptors.FileDescriptor fileDescriptor = request.getFileDescriptor(fileName);
            if (fileDescriptor == null) {
                throw new GeneratorException("file descriptor '" + fileName + "' not found");
            }
            generateCodeForDescriptor(fileDescriptor, result);
        }
        return result;
    }

    private void generateCodeForDescriptor(Descriptors.FileDescriptor fileDescriptor, DefaultGeneratorResult result) throws Exception {
        STGroup group = new STGroupFile("com/kftech/prolo/codegen/templates/main.stg");
        group.registerModelAdaptor(Descriptors.FieldDescriptor.class, new FieldDescriptorAdaptor());
        group.registerModelAdaptor(Descriptors.FileDescriptor.class, new FileDescriptorAdaptor());
        group.registerModelAdaptor(Descriptors.Descriptor.class, new DescriptorAdaptor());
        group.registerModelAdaptor(Descriptors.EnumDescriptor.class, new EnumDescriptorAdaptor());

        ST outerClass = group.getInstanceOf("outerClass");
        outerClass.add("fd", fileDescriptor);
        String contents = outerClass.render();
        result.addGeneratedFile(toGeneratedFilePath(fileDescriptor), contents);
    }

    private static String toGeneratedFilePath(Descriptors.FileDescriptor fileDescriptor) {
        String packagePath = Utils.getPackageName(fileDescriptor).replace('.', '/');
        String fileName = Utils.getOuterClassName(fileDescriptor) + ".java";
        return packagePath + '/' + fileName;
    }
}
