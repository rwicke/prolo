package org.kftech.prolo.codegen;

import com.google.protobuf.Descriptors;
import org.stringtemplate.v4.Interpreter;
import org.stringtemplate.v4.ST;

/**
 * Extends {@link Descriptors.FileDescriptor} with additional properties for use in String Template.
 */
public class FileDescriptorAdaptor extends ObjectExtensionAdaptor<Descriptors.FileDescriptor> {
    public FileDescriptorAdaptor() {
        addExtension("javaPackageName", new ExtensionFunction<Descriptors.FileDescriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.FileDescriptor o, String propertyName) {
                return Utils.getPackageName(o);
            }
        });
        addExtension("javaOuterClassName", new ExtensionFunction<Descriptors.FileDescriptor>() {
            public Object call(Interpreter interp, ST self, Descriptors.FileDescriptor o, String propertyName) {
                return Utils.getOuterClassName(o);
            }
        });
        addExtension("extensionDescriptors", new ExtensionFunction<Descriptors.FileDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.FileDescriptor o, String propertyName) {
                return ExtensionDescriptor.getExtensionDescriptors(o);
            }
        });
    }
}
