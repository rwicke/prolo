package org.kftech.prolo.codegen;

import com.google.protobuf.Descriptors;
import org.stringtemplate.v4.Interpreter;
import org.stringtemplate.v4.ST;

/**
 * Extends {@link Descriptors.EnumDescriptor} with additional properties for use in String Template.
 */
public class EnumDescriptorAdaptor  extends ObjectExtensionAdaptor<Descriptors.EnumDescriptor> {
    public EnumDescriptorAdaptor() {
        addExtension("fqName", new ExtensionFunction<Descriptors.EnumDescriptor>() {
            @Override
            public Object call(Interpreter interp, ST self, Descriptors.EnumDescriptor o, String propertyName) {
                return Utils.getFullyQualifiedEnumName(o);
            }
        });
    }
}
