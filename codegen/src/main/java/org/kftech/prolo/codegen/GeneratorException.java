package org.kftech.prolo.codegen;

/**
 * Exception thrown by the code generator.
 */
public class GeneratorException extends Exception {
    public GeneratorException(String message) {
        super(message);
    }

    public GeneratorException(String message, Throwable cause) {
        super(message, cause);
    }
}
