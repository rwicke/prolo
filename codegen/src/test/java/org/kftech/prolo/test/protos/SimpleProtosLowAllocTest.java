package org.kftech.prolo.test.protos;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.CodedOutputStream;
import org.junit.Test;
import org.kftech.prolo.ProtobufDecoder;
import org.kftech.prolo.ProtobufEncoder;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.kftech.prolo.test.protos.SimpleProtosLowAlloc.Minimal;

public class SimpleProtosLowAllocTest {

    @Test
    public void createMinimalMessage() {

        Minimal minimal = Minimal.newInstance();
        assertNotNull(minimal);
    }

    @Test
    public void getAndSetValues() {
        Minimal minimal = Minimal.newInstance();
        minimal.setField1(1);
        assertEquals(1, minimal.getField1());
        minimal.setField2(2);
        assertEquals(2, minimal.getField2());

        minimal.setField1(10);
        assertEquals(10, minimal.getField1());
        minimal.setField2(20);
        assertEquals(20, minimal.getField2());
    }

    @Test
    public void validateCompleteState() {
        Minimal minimal = Minimal.newInstance();
        minimal.setField1(100);
        minimal.validate();
    }

    @Test(expected = IllegalStateException.class)
    public void validateIncompleteState() {
        Minimal minimal = Minimal.newInstance();
        minimal.setField2(100);
        minimal.validate();
    }

    @Test(expected = IllegalStateException.class)
    public void clearInvalidatesState() {
        Minimal minimal = Minimal.newInstance();
        minimal.setField1(100);
        minimal.clear();
        minimal.getField1();
    }

    @Test
    public void calculateSerializedSize() {
        Minimal minimal = Minimal.newInstance();
        minimal.setField1(1);
        int size = minimal.getSerializedSize();
        assertTrue(size > 0);
        minimal.setField2(1);
        assertTrue(minimal.getSerializedSize() > size);
        minimal.clear();
        minimal.setField1(1);
        assertEquals(size, minimal.getSerializedSize());
    }

    @Test
    public void serializeAndDeserializeWithLowAlloc() throws Exception {

        byte[] buffer = new byte[1024];
        ProtobufEncoder encoder = new ProtobufEncoder(buffer);
        assertEquals(0, encoder.getPosition());
        Minimal minimal = Minimal.newInstance();
        minimal.setField1(10);
        minimal.setField2(20);
        minimal.writeTo(encoder);
        int length = encoder.getPosition();
        assertTrue(length > 0);
        ProtobufDecoder decoder = new ProtobufDecoder(buffer);
        decoder.setMessageSize(length);
        Minimal minimal2 = Minimal.newInstance();
        minimal2.readFrom(decoder);
        minimal2.validate();
        assertEquals(minimal.getField1(), minimal2.getField1());
        assertEquals(minimal.getField2(), minimal2.getField2());
    }

    @Test
    public void protobufToProlo() throws IOException {
        byte[] buffer = new byte[256];
        SimpleProtos.Minimal mproto = SimpleProtos.Minimal.newBuilder().setField1(123).setField2(345023984).build();
        CodedOutputStream os = CodedOutputStream.newInstance(buffer);
        int size = mproto.getSerializedSize();
        mproto.writeTo(os);

        Minimal mprolo = Minimal.newInstance();
        ProtobufDecoder decoder = new ProtobufDecoder(buffer);
        decoder.setMessageSize(size);
        mprolo.readFrom(decoder);

        assertEquals(mproto.getSerializedSize(), mprolo.getSerializedSize());
        assertEquals(mproto.getField1(), mprolo.getField1());
        assertEquals(mproto.getField2(), mprolo.getField2());
    }

    @Test
    public void proloToProtobuf() throws IOException {
        Minimal mprolo = Minimal.newInstance();
        mprolo.setField1(234098);
        mprolo.setField2(23498329);
        int size = mprolo.getSerializedSize();
        byte[] buffer = new byte[256];
        ProtobufEncoder encoder = new ProtobufEncoder(buffer);
        mprolo.writeTo(encoder);
        assertEquals(size, encoder.getMessageSize());

        SimpleProtos.Minimal.Builder builder = SimpleProtos.Minimal.newBuilder();
        CodedInputStream in = CodedInputStream.newInstance(buffer, 0, size);
        builder.mergeFrom(in);
        SimpleProtos.Minimal mproto = builder.build();
        assertEquals(mprolo.getField1(), mproto.getField1());
        assertEquals(mprolo.getField2(), mproto.getField2());
        assertEquals(size, mproto.getSerializedSize());
    }
}
