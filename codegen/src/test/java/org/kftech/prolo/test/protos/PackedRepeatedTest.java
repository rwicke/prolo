package org.kftech.prolo.test.protos;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.CodedOutputStream;
import org.junit.Test;
import org.kftech.prolo.ProtobufDecoder;
import org.kftech.prolo.ProtobufEncoder;

import static org.junit.Assert.assertEquals;

public class PackedRepeatedTest {

    @Test
    public void testEncodeProloDecodeProtobuf() throws Exception {
        AllTypesLowAlloc.PackedRepeated pr = AllTypesLowAlloc.PackedRepeated.newInstance();
        pr.addId(1);
        pr.addId(100);
        pr.addId(1000);

        byte[] buffer = new byte[1024];
        ProtobufEncoder encoder = new ProtobufEncoder(buffer);
        pr.writeTo(encoder);
        int size = encoder.getMessageSize();
        CodedInputStream in = CodedInputStream.newInstance(buffer, 0, size);
        AllTypes.PackedRepeated prProto = AllTypes.PackedRepeated
                .newBuilder().mergeFrom(in).build();
        assertEquals(pr.getIdCount(), prProto.getIdCount());
        for (int i = 0; i < pr.getIdCount(); i++) {
            assertEquals(pr.getId(i), prProto.getId(i));
        }
    }

    @Test
    public void testEncodeProtobufDecodeProlo() throws Exception {
        AllTypes.PackedRepeated.Builder prBuilder = AllTypes.PackedRepeated.newBuilder();
        prBuilder.addId(1);
        prBuilder.addId(100);
        prBuilder.addId(1000);

        byte[] buffer = new byte[1024];
        CodedOutputStream out = CodedOutputStream.newInstance(buffer);
        AllTypes.PackedRepeated prProto = prBuilder.build();
        prProto.writeTo(out);
        ProtobufDecoder decoder = new ProtobufDecoder(buffer);
        decoder.setMessageSize(prProto.getSerializedSize());
        AllTypesLowAlloc.PackedRepeated pr = AllTypesLowAlloc.PackedRepeated.newInstance();
        pr.readFrom(decoder);

        assertEquals(prProto.getIdCount(), pr.getIdCount());
        for (int i = 0; i < prProto.getIdCount(); i++) {
            assertEquals(prProto.getId(i), pr.getId(i));
        }

    }

}
