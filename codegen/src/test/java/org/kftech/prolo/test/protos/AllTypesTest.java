package org.kftech.prolo.test.protos;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.CodedOutputStream;
import org.junit.Test;
import org.kftech.prolo.ProtobufDecoder;
import org.kftech.prolo.ProtobufEncoder;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class AllTypesTest {

    @Test
    public void testPackedInt32ProtobufToProlo() throws Exception {
        byte[] buffer = new byte[1024];

        AllTypes.PackedRepeated proto = AllTypes.PackedRepeated.newBuilder().addAllId(Arrays.asList(1, 2, 3, 4)).build();
        CodedOutputStream os = CodedOutputStream.newInstance(buffer);
        int size = proto.getSerializedSize();
        proto.writeTo(os);

        AllTypesLowAlloc.PackedRepeated prolo = AllTypesLowAlloc.PackedRepeated.newInstance();
        ProtobufDecoder decoder = new ProtobufDecoder(buffer);
        decoder.setMessageSize(size);
        prolo.readFrom(decoder);

        compareProloToProtobuf(prolo, proto);
    }

    @Test
    public void testPackedInt32ProloToProtobuf() throws Exception {
        byte[] buffer = new byte[1024];

        AllTypesLowAlloc.PackedRepeated prolo = AllTypesLowAlloc.PackedRepeated.newInstance();
        for (int i = 0; i < 5; i++) {
            prolo.addId(i + 1);
        }
        int size = prolo.getSerializedSize();
        ProtobufEncoder encoder = new ProtobufEncoder(buffer);
        prolo.writeTo(encoder);
        CodedInputStream in = CodedInputStream.newInstance(buffer, 0, size);
        AllTypes.PackedRepeated.Builder protoBuilder = AllTypes.PackedRepeated.newBuilder();
        protoBuilder.mergeFrom(in);
        AllTypes.PackedRepeated proto = protoBuilder.build();

        compareProloToProtobuf(prolo, proto);



    }

    private void compareProloToProtobuf(AllTypesLowAlloc.PackedRepeated prolo, AllTypes.PackedRepeated proto) {
        assertEquals(prolo.getIdCount(), proto.getIdCount());

        for (int i = 0; i < prolo.getIdCount(); i++) {
            assertEquals(prolo.getId(i), proto.getId(i));
        }
    }
}
