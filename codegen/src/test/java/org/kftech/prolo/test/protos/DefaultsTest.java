package org.kftech.prolo.test.protos;

import org.junit.Before;
import org.junit.Test;
import org.kftech.prolo.MutableAsciiString;
import org.kftech.prolo.MutableByteString;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DefaultsTest {

    private static final float EPSILON = 0.00001f;

    DefaultsProtos.MessageWithDefaults proto;
    DefaultsProtosLowAlloc.MessageWithDefaults prolo;

    @Before
    public void setUp() {
        proto = DefaultsProtos.MessageWithDefaults.newBuilder().build();
        prolo = DefaultsProtosLowAlloc.MessageWithDefaults.newInstance();
    }


    @Test
    public void testInt32Defaults() {
        assertEquals(proto.getInt32WithDefault(), prolo.getInt32WithDefault());
        assertEquals(proto.getUint32WithDefault(), prolo.getUint32WithDefault());
        assertEquals(proto.getSint32WithDefault(), prolo.getSint32WithDefault());
        assertEquals(proto.getFixed32WithDefault(), prolo.getFixed32WithDefault());
        assertEquals(proto.getSfixed32WithDefault(), prolo.getSfixed32WithDefault());
    }

    @Test
    public void testInt64Defaults() {
        assertEquals(proto.getInt64WithDefault(), prolo.getInt64WithDefault());
        assertEquals(proto.getUint64WithDefault(), prolo.getUint64WithDefault());
        assertEquals(proto.getSint64WithDefault(), prolo.getSint64WithDefault());
        assertEquals(proto.getFixed64WithDefault(), prolo.getFixed64WithDefault());
        assertEquals(proto.getSfixed64WithDefault(), prolo.getSfixed64WithDefault());
    }

    @Test
    public void testBoolDefaults() {
        assertEquals(proto.getBoolWithDefault(), prolo.getBoolWithDefault());
    }

    @Test
    public void testStringWithDefaults() {
        MutableAsciiString expected = new MutableAsciiString();
        expected.copyFrom(proto.getStringWithDefault());
        assertEquals(expected, prolo.getStringWithDefault());
    }

    @Test
    public void testBytesWithDefaults() {
        MutableByteString expected = new MutableByteString();
        expected.copyFrom(proto.getBytesWithDefault().toByteArray());
        assertEquals(expected, prolo.getBytesWithDefault());
    }

    @Test
    public void testEnumWithDefaults() {
        assertEquals(proto.getEnumWithDefault().getNumber(), prolo.getEnumWithDefault().getNumber());
    }

    @Test
    public void testFloatWithDefaults() {
        assertEquals(proto.getFloatWithDefault(), prolo.getFloatWithDefault(), EPSILON);
        assertEquals(proto.getFloatWithDefaultInf(), prolo.getFloatWithDefaultInf(), EPSILON);
        assertEquals(proto.getFloatWithDefaultNegInf(), prolo.getFloatWithDefaultNegInf(), EPSILON);
        assertTrue(Float.isNaN(prolo.getFloatWithDefaultNaN()));
    }


}
