package org.kftech.prolo.test.protos;


import org.junit.Test;
import org.kftech.prolo.ProtobufDecoder;
import org.kftech.prolo.ProtobufEncoder;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.kftech.prolo.test.protos.SimpleProtosLowAlloc.Nested;

public class NestedProtosTest {

    @Test
    public void createRecursiveStructure() {
        Nested first = Nested.newInstance();
        first.setId(1);
        Nested second = first.newChild();
        second.setId(2);
        Nested third = second.newChild();
        third.setId(3);
        Nested fourth = third.newChild();
        fourth.setId(4);
        Nested fifth = fourth.newChild();
        first.validate();


        assertTrue(first.hasChild());
        assertTrue(second.hasChild());
        assertTrue(third.hasChild());
        assertTrue(fourth.hasChild());
        assertFalse(fifth.hasChild());

        assertEquals(3, first.getChild().getChild().getId());
    }

    @Test
    public void serialiseNested() throws IOException {
        Nested outer = Nested.newInstance();
        outer.setId(0);
        Nested current = outer;
        for (int i = 1; i < 4; i++) {
            current = current.newChild();
            current.setId(i);
        }

        byte[] buffer = new byte[1024];
        ProtobufEncoder encoder = new ProtobufEncoder(buffer);
        ProtobufDecoder decoder = new ProtobufDecoder(buffer);

        outer.writeTo(encoder);

        Nested outerCopy = Nested.newInstance();
        decoder.setMessageSize(encoder.getMessageSize());
        outerCopy.readFrom(decoder);

        current = outer;
        Nested currentCopy = outerCopy;

        while (current.hasChild()) {
            assertEquals(current.getId(), currentCopy.getId());
            assertTrue(outerCopy.hasChild());
            current = current.getChild();
            currentCopy = currentCopy.getChild();
        }
        assertFalse(currentCopy.hasChild());


    }



}
