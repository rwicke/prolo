package org.kftech.prolo.test.protos;


import org.junit.Before;
import org.junit.Test;
import org.kftech.prolo.ExtensionContext;
import org.kftech.prolo.ProtobufDecoder;
import org.kftech.prolo.ProtobufEncoder;

import static org.junit.Assert.*;
import static org.kftech.prolo.test.protos.PersonProtosLowAlloc.*;

public class ExtensionTest {

    private Person person;
    private ExtensionContext ctx;

    @Before
    public void createPerson() {
        ctx = new ExtensionContext();
        ctx.registerExtension(Person.class, PersonExtension.class);
        person = Person.newInstance(ctx);
    }

    @Test
    public void createPersonWithExtension() {
        person.setAge(25);
        person.setName("John");
        person.setGender(Gender.MALE);

        int size = person.getSerializedSize();
        PersonExtension personExt = person.getExtension(PersonExtension.class);
        assertNotNull(personExt);
        personExt.setHeight(180);
        assertTrue(person.getSerializedSize() > size);
    }

    @Test
    public void serialiseWithExtension() throws Exception {
        person = Person.newInstance(ctx);
        person.setAge(25);
        person.setName("John");
        person.setGender(Gender.MALE);
        person.getExtension(PersonExtension.class).setHeight(180);

        byte[] buffer = new byte[1024];
        ProtobufEncoder encoder = new ProtobufEncoder(buffer);
        ProtobufDecoder decoder = new ProtobufDecoder(buffer);
        person.writeTo(encoder);

        Person personCopy = Person.newInstance(ctx);
        decoder.setMessageSize(encoder.getMessageSize());
        personCopy.readFrom(decoder);
        assertEquals(25, personCopy.getAge());
        assertEquals("John", personCopy.getName().toString());
        assertEquals(Gender.MALE, personCopy.getGender());
        assertNotNull(personCopy.getExtension(PersonExtension.class));
        assertTrue(personCopy.getExtension(PersonExtension.class).hasHeight());
        assertEquals(180, personCopy.getExtension(PersonExtension.class).getHeight());
    }

}
