package org.kftech.prolo.test.protos;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.CodedOutputStream;
import org.junit.Test;
import org.kftech.prolo.ProtobufDecoder;
import org.kftech.prolo.ProtobufEncoder;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.kftech.prolo.test.protos.PersonProtosLowAlloc.Gender;
import static org.kftech.prolo.test.protos.PersonProtosLowAlloc.Person;

public class PersonTest {

    @Test
    public void serialisePerson() throws IOException {
        Person john = buildJohnProlo();

        byte[] buffer = new byte[1024];
        ProtobufEncoder encoder = new ProtobufEncoder(buffer);
        ProtobufDecoder decoder = new ProtobufDecoder(buffer);
        john.writeTo(encoder);


        assertEquals(john.getSerializedSize(), encoder.getMessageSize());

        decoder.setMessageSize(john.getSerializedSize());
        Person johnCopy = Person.newInstance();
        johnCopy.readFrom(decoder);

        assertEquals(25, johnCopy.getAge());
        assertEquals("John", johnCopy.getName().toString());
        assertEquals(Gender.MALE, johnCopy.getGender());

        assertTrue(john.hasSpouse());
        Person aliceCopy = john.getSpouse();
        assertEquals(1, aliceCopy.getPreviousGenderCount());
        assertEquals(Gender.MALE, aliceCopy.getPreviousGender(0));
        assertEquals(2, aliceCopy.getLuckyNumberCount());
        assertEquals(8, aliceCopy.getLuckyNumber(0));
        assertEquals(7, aliceCopy.getLuckyNumber(1));
        assertEquals(2, aliceCopy.getFriendCount());
        assertEquals("Bob", aliceCopy.getFriend(0).getName().toString());
        assertEquals("Fred", aliceCopy.getFriend(1).getName().toString());
        assertFalse(aliceCopy.hasPreviousToken());
        assertFalse(aliceCopy.hasToken());
        assertFalse(aliceCopy.hasMiddleName());

        assertEquals("Alexander", aliceCopy.getFriend(1).getMiddleName(0).toString());
        assertEquals("Blaise", aliceCopy.getFriend(1).getMiddleName(1).toString());
        assertEquals("Baptiste", aliceCopy.getFriend(1).getMiddleName(2).toString());
    }

    @Test
    public void protobufToProlo() throws Exception {
        byte[] buffer = new byte[1024];
        PersonProtos.Person johnProtobuf = buildJohnProtobuf();
        CodedOutputStream os = CodedOutputStream.newInstance(buffer);
        int size = johnProtobuf.getSerializedSize();
        johnProtobuf.writeTo(os);

        Person johnProlo = Person.newInstance();
        ProtobufDecoder decoder = new ProtobufDecoder(buffer);
        decoder.setMessageSize(size);
        johnProlo.readFrom(decoder);

        compareProloToProtobuf(johnProtobuf, johnProlo);
    }

    @Test
    public void proloToProtobuf() throws Exception {
        byte[] buffer = new byte[1024];
        Person johnProlo = buildJohnProlo();
        int size = johnProlo.getSerializedSize();
        ProtobufEncoder encoder = new ProtobufEncoder(buffer);
        johnProlo.writeTo(encoder);
        assertEquals(size, encoder.getMessageSize());
        CodedInputStream in = CodedInputStream.newInstance(buffer, 0, size);
        PersonProtos.Person.Builder b = PersonProtos.Person.newBuilder();
        b.mergeFrom(in);
        PersonProtos.Person johnProtobuf = b.build();
        compareProloToProtobuf(johnProtobuf, johnProlo);
    }


    private void compareProloToProtobuf(PersonProtos.Person protobuf, Person prolo) {
        assertEquals(protobuf.getAge(), prolo.getAge());
        assertEquals(protobuf.getGender().getNumber(), prolo.getGender().getNumber());
        assertEquals(protobuf.hasName(), prolo.hasName());
        if (protobuf.hasName()) {
            assertEquals(protobuf.getName(), prolo.getName().toString());
        }
        assertEquals(protobuf.hasSpouse(), prolo.hasSpouse());
        if (protobuf.hasSpouse()) {
            compareProloToProtobuf(protobuf.getSpouse(), prolo.getSpouse());
        }
        assertEquals(protobuf.getLuckyNumberCount(), prolo.getLuckyNumberCount());
        for (int i = 0; i < protobuf.getLuckyNumberCount(); i++) {
            assertEquals(protobuf.getLuckyNumber(i), prolo.getLuckyNumber(i));
        }
        assertEquals(protobuf.getPreviousGenderCount(), prolo.getPreviousGenderCount());
        for (int i = 0; i < protobuf.getPreviousGenderCount(); i++) {
            assertEquals(protobuf.getPreviousGender(i).getNumber(), prolo.getPreviousGender(i).getNumber());
        }
        assertEquals(protobuf.getFriendCount(), prolo.getFriendCount());
        for (int i = 0; i < protobuf.getFriendCount(); i++) {
            compareProloToProtobuf(protobuf.getFriend(i), prolo.getFriend(i));
        }
        assertEquals(protobuf.getMiddleNameCount(), prolo.getMiddleNameCount());
        for (int i = 0; i < protobuf.getMiddleNameCount(); i++) {
            assertEquals(protobuf.getMiddleName(i), prolo.getMiddleName(i).toString());
        }
    }


    private PersonProtos.Person buildJohnProtobuf() {


        PersonProtos.Person bob = PersonProtos.Person.newBuilder()
                .setName("Bob")
                .setAge(27)
                .setGender(PersonProtos.Gender.MALE).build();

        PersonProtos.Person fred = PersonProtos.Person.newBuilder()
                .setAge(29)
                .setName("Fred")
                .setGender(PersonProtos.Gender.MALE)
                .addMiddleName("Alexander")
                .addMiddleName("Blaise")
                .addMiddleName("Baptiste").build();

        PersonProtos.Person alice = PersonProtos.Person.newBuilder()
                .setAge(24)
                .setName("Alice")
                .setGender(PersonProtos.Gender.FEMALE)
                .addLuckyNumber(8)
                .addLuckyNumber(7)
                .addPreviousGender(PersonProtos.Gender.MALE)
                .addFriend(bob)
                .addFriend(fred).build();

        PersonProtos.Person john = PersonProtos.Person.newBuilder()
                .setAge(25)
                .setName("John")
                .setGender(PersonProtos.Gender.MALE)
                .setSpouse(alice).build();

        return john;
    }

    private Person buildJohnProlo() {
        Person john = Person.newInstance();
        john.setAge(25);
        john.setName("John");
        john.setGender(PersonProtosLowAlloc.Gender.MALE);

        Person alice = john.newSpouse();
        alice.setAge(24);
        alice.setGender(Gender.FEMALE);
        alice.setName("Alice");
        alice.addLuckyNumber(8);
        alice.addLuckyNumber(7);
        alice.addPreviousGender(Gender.MALE);

        Person bob = alice.addFriend();
        bob.setName("Bob");
        bob.setAge(27);
        bob.setGender(Gender.MALE);

        Person fred = alice.addFriend();
        fred.setName("Fred");
        fred.setAge(29);
        fred.setGender(Gender.MALE);
        fred.addMiddleName("Alexander");
        fred.addMiddleName("Blaise");
        fred.addMiddleName("Baptiste");
        return john;
    }
}
