package org.kftech.prolo.codegen;

import com.google.protobuf.Descriptors;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created with IntelliJ IDEA.
 * User: rwicke
 * Date: 20/08/2012
 * Time: 21:57
 * To change this template use File | Settings | File Templates.
 */
public class FileDescriptorSetGeneratorRequestTest {

    private static final String PROTO_FILE_NAME = "unittest/include.proto";
    private static final String INCLUDE_FILE_NAME = "unittest/common/base.proto";

    private File fileSetProtoFile;

    @Before
    public void setUp() throws Exception {
//        URL url = getClass().getResource("/descriptorset.protobin");
        fileSetProtoFile = new File("target/generated-test-resources/protobuf/descriptor-sets/descriptorset.protobin");
    }

    @Test
    public void testGeneratorRequest() {

        List<String> fileNames = new ArrayList<String>();
        fileNames.add(PROTO_FILE_NAME);
        FileDescriptorSetGeneratorRequest request = new FileDescriptorSetGeneratorRequest(fileNames, fileSetProtoFile);
        assertEquals(1, request.getFilesToGenerate().size());
        assertEquals(PROTO_FILE_NAME, request.getFilesToGenerate().get(0));
        Descriptors.FileDescriptor file = request.getFileDescriptor(PROTO_FILE_NAME);
        assertNotNull(file);
        assertEquals(PROTO_FILE_NAME, file.getName());
        assertEquals(1, file.getDependencies().size());
        assertEquals(INCLUDE_FILE_NAME, file.getDependencies().get(0).getName());
    }
}
