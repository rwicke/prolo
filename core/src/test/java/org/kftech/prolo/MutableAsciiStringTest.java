package org.kftech.prolo;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MutableAsciiStringTest {

    @Test
    public void testCopyFromCharSequence() {
        MutableAsciiString str = new MutableAsciiString(16);
        assertEquals(0, str.length());
        str.copyFrom("John");
        assertEquals(4, str.length());

        str.copyFrom("Foo");
        assertEquals(3, str.length());

        str.copyFrom("prolo".getBytes());
        assertEquals("prolo".length(), str.length());
        assertEquals("prolo", str.toString());
    }
}
