package org.kftech.prolo;

import java.io.IOException;

/**
 * Decodes messages in {@code protobuf} serialized form.
 *
 * This class is based on, and borrows heavily from, {@code CodedInputStream} in Google's
 * {@code protobuf} library.
 */
public class ProtobufDecoder {

    private byte[] buffer;
    private int startPos;
    private int capacity;
    private int bufferPos;
    private int limit;

    private int lastTag;

    private int recursionDepth;
    // TODO: make recursionLimit configurable
    private int recursionLimit = 64;

    public ProtobufDecoder(byte[] buffer) {
        this(buffer, 0, buffer.length);
    }

    public ProtobufDecoder(byte[] buffer, int pos, int len) {
        this.buffer = buffer;
        this.startPos = pos;
        this.capacity = len;
        clear();
    }

    public void setBuffer(byte[] buffer, int offset, int length) {
        this.buffer = buffer;
        this.startPos = offset;
        this.capacity = length;
        clear();
    }

    public void setMessageSize(int length) {
        this.capacity = length;
        clear();
    }

    public void clear() {
        this.bufferPos = startPos;
        this.limit = startPos + capacity;
    }

    public int pushLimit(int byteLimit) throws IOException {
        if (byteLimit < 0) {
            throw new IOException("negative byteLimit");
        }
        final int newLimit = bufferPos + byteLimit;
        final int oldLimit = limit;
        if (newLimit > oldLimit) {
            throw new IOException("truncated message");
        }
        limit = newLimit;
        return oldLimit;
    }

    public void popLimit(final int oldLimit) {
        limit = oldLimit;
    }

    public int position() {
        return bufferPos;
    }

    public int limit() {
        return limit;
    }

    /**
     * Attempt to read a field tag, returning zero if we have reached EOF.
     * Protocol message parsers use this to read tags, since a protocol message
     * may legally end wherever a tag occurs, and zero is not a valid tag number.
     */
    public int readTag() throws IOException {
        if (isAtEnd()) {
            lastTag = 0;
            return 0;
        }

        lastTag = readRawVarint32();
        if (WireFormatHelper.getTagFieldNumber(lastTag) == 0) {
            // If we actually read zero (or any tag number corresponding to field
            // number zero), that's not a valid tag.
            throw new IOException("invalid tag");
        }
        return lastTag;
    }

    /**
     * Verifies that the last call to readTag() returned the given tag value.
     * This is used to verify that a nested group ended with the correct
     * end tag.
     *
     * @throws IOException {@code value} does not match the
     *                                        last tag.
     */
    public void checkLastTagWas(final int value)
            throws IOException {
        if (lastTag != value) {
            throw new IOException("invalid end tag");
        }
    }

    /**
     * Reads and discards a single field, given its tag value.
     *
     * @return {@code false} if the tag is an endgroup tag, in which case
     *         nothing is skipped.  Otherwise, returns {@code true}.
     */
    public boolean skipField(final int tag) throws IOException {
        switch (WireFormatHelper.getTagWireType(tag)) {
        case WireFormatHelper.WIRETYPE_VARINT:
            readInt32();
            return true;
        case WireFormatHelper.WIRETYPE_FIXED64:
            readRawLittleEndian64();
            return true;
        case WireFormatHelper.WIRETYPE_LENGTH_DELIMITED:
            skipRawBytes(readRawVarint32());
            return true;
        case WireFormatHelper.WIRETYPE_FIXED32:
            readRawLittleEndian32();
            return true;
        default:
            throw new IOException("invalid wire format type");
        }
    }

    public void skipRawBytes(final int size) throws IOException {
        if (size < 0) {
            throw new IOException("negative size");
        }

        if (bufferPos + size >= limit) {
            bufferPos = limit;
            throw new IOException("truncated message");
        }

        bufferPos += size;
    }


    /** Read a {@code double} field value from the stream. */
    public double readDouble() throws IOException {
        return Double.longBitsToDouble(readRawLittleEndian64());
    }

    /** Read a {@code float} field value from the stream. */
    public float readFloat() throws IOException {
        return Float.intBitsToFloat(readRawLittleEndian32());
    }

    /** Read a {@code uint64} field value from the stream. */
    public long readUInt64() throws IOException {
        return readRawVarint64();
    }

    /** Read an {@code int64} field value from the stream. */
    public long readInt64() throws IOException {
        return readRawVarint64();
    }

    /** Read an {@code int32} field value from the stream. */
    public int readInt32() throws IOException {
        return readRawVarint32();
    }

    /** Read a {@code fixed64} field value from the stream. */
    public long readFixed64() throws IOException {
        return readRawLittleEndian64();
    }

    /** Read a {@code fixed32} field value from the stream. */
    public int readFixed32() throws IOException {
        return readRawLittleEndian32();
    }

    /** Read a {@code bool} field value from the stream. */
    public boolean readBool() throws IOException {
        return readRawVarint32() != 0;
    }

    /** Read a {@code uint32} field value from the stream. */
    public int readUInt32() throws IOException {
        return readRawVarint32();
    }

    /**
     * Read an enum field value from the stream.  Caller is responsible
     * for converting the numeric value to an actual enum.
     */
    public int readEnum() throws IOException {
        return readRawVarint32();
    }

    /** Read an {@code sfixed32} field value from the stream. */
    public int readSFixed32() throws IOException {
        return readRawLittleEndian32();
    }

    /** Read an {@code sfixed64} field value from the stream. */
    public long readSFixed64() throws IOException {
        return readRawLittleEndian64();
    }

    /** Read an {@code sint32} field value from the stream. */
    public int readSInt32() throws IOException {
        return decodeZigZag32(readRawVarint32());
    }

    /** Read an {@code sint64} field value from the stream. */
    public long readSInt64() throws IOException {
        return decodeZigZag64(readRawVarint64());
    }

    public void readMessage(ProtobufMessage message) throws IOException {
        final int length = readRawVarint32();
        if (recursionDepth >= recursionLimit) {
            throw new IOException("recursion limit exceeded");
        }
        final int oldLimit = pushLimit(length);
        ++recursionDepth;
        message.readFrom(this);
        --recursionDepth;
        popLimit(oldLimit);
    }

    public void readString(MutableAsciiString string) throws IOException {
        final int size = readRawVarint32();
        if (size == 0) {
            string.clear();
            return;
        } else if (size > 0 && bufferPos + size <= limit) {
            string.copyFrom(buffer, bufferPos, size);
            bufferPos += size;
        } else {
            throw new IOException("string exceeds buffer capacity");
        }
    }

    public void readBytes(MutableByteString bytes) throws IOException {
        final int size = readRawVarint32();
        if (size == 0) {
            bytes.clear();
            return;
        } else if (size > 0 && bufferPos + size <= limit) {
            bytes.copyFrom(buffer, bufferPos, size);
            bufferPos += size;
        } else {
            throw new IOException("byte string exceeds buffer capacity");
        }
    }

    public byte readRawByte() {
        return buffer[bufferPos++];
    }

    /**
     * Read a raw Varint from the stream. If larger than 32 bits, discard the
     * upper bits.
     */
    public int readRawVarint32() throws IOException {
        byte tmp = readRawByte();
        if (tmp >= 0) {
            return tmp;
        }
        int result = tmp & 0x7f;
        if ((tmp = readRawByte()) >= 0) {
            result |= tmp << 7;
        } else {
            result |= (tmp & 0x7f) << 7;
            if ((tmp = readRawByte()) >= 0) {
                result |= tmp << 14;
            } else {
                result |= (tmp & 0x7f) << 14;
                if ((tmp = readRawByte()) >= 0) {
                    result |= tmp << 21;
                } else {
                    result |= (tmp & 0x7f) << 21;
                    result |= (tmp = readRawByte()) << 28;
                    if (tmp < 0) {
                        // Discard upper 32 bits.
                        for (int i = 0; i < 5; i++) {
                            if (readRawByte() >= 0) {
                                return result;
                            }
                        }
                        throw new IOException("malformed varint");
                    }
                }
            }
        }
        return result;
    }

    /** Read a raw Varint from the stream. */
    public long readRawVarint64() throws IOException {
        int shift = 0;
        long result = 0;
        while (shift < 64) {
            final byte b = readRawByte();
            result |= (long)(b & 0x7F) << shift;
            if ((b & 0x80) == 0) {
                return result;
            }
            shift += 7;
        }
        throw new IOException("malformed varint");
    }

    /** Read a 32-bit little-endian integer from the stream. */
    public int readRawLittleEndian32() throws IOException {
        final byte b1 = readRawByte();
        final byte b2 = readRawByte();
        final byte b3 = readRawByte();
        final byte b4 = readRawByte();
        return ((b1 & 0xff)      ) |
                ((b2 & 0xff) <<  8) |
                ((b3 & 0xff) << 16) |
                ((b4 & 0xff) << 24);
    }

    /** Read a 64-bit little-endian integer from the stream. */
    public long readRawLittleEndian64() throws IOException {
        final byte b1 = readRawByte();
        final byte b2 = readRawByte();
        final byte b3 = readRawByte();
        final byte b4 = readRawByte();
        final byte b5 = readRawByte();
        final byte b6 = readRawByte();
        final byte b7 = readRawByte();
        final byte b8 = readRawByte();
        return (((long)b1 & 0xff)      ) |
                (((long)b2 & 0xff) <<  8) |
                (((long)b3 & 0xff) << 16) |
                (((long)b4 & 0xff) << 24) |
                (((long)b5 & 0xff) << 32) |
                (((long)b6 & 0xff) << 40) |
                (((long)b7 & 0xff) << 48) |
                (((long)b8 & 0xff) << 56);
    }

    /**
     * Decode a ZigZag-encoded 32-bit value.  ZigZag encodes signed integers
     * into values that can be efficiently encoded with varint.  (Otherwise,
     * negative values must be sign-extended to 64 bits to be varint encoded,
     * thus always taking 10 bytes on the wire.)
     *
     * @param n An unsigned 32-bit integer, stored in a signed int because
     *          Java has no explicit unsigned support.
     * @return A signed 32-bit integer.
     */
    public static int decodeZigZag32(final int n) {
        return (n >>> 1) ^ -(n & 1);
    }

    /**
     * Decode a ZigZag-encoded 64-bit value.  ZigZag encodes signed integers
     * into values that can be efficiently encoded with varint.  (Otherwise,
     * negative values must be sign-extended to 64 bits to be varint encoded,
     * thus always taking 10 bytes on the wire.)
     *
     * @param n An unsigned 64-bit integer, stored in a signed int because
     *          Java has no explicit unsigned support.
     * @return A signed 64-bit integer.
     */
    public static long decodeZigZag64(final long n) {
        return (n >>> 1) ^ -(n & 1);
    }

    public boolean isAtEnd() {
        return bufferPos == limit;
    }

}
