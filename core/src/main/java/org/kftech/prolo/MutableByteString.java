package org.kftech.prolo;

import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * Mutable byte string implementation.
 */
public final class MutableByteString implements ByteSequence {

    private static final Charset ASCII_CHARSET;
    static {
        ASCII_CHARSET = Charset.forName("US-ASCII");
    }

    private byte[] bytes;
    private int length;

    /**
     * Creates a byte string with given initial capacity.
     * @param capacity
     */
    public MutableByteString(int capacity) {
        this.bytes = new byte[capacity];
        this.length = 0;
    }

    /**
     * Creates a byte string with default initial capacity.
     */
    public MutableByteString() {
        this(32);
    }

    /**
     * Copies bytes from another buffer. The capacity of this sequence will be extended,
     * if necessary, to accommodate the given byte range.
     * @param buffer buffer to copy from
     * @param start index of first byte in {@code buffer}
     * @param length length of bytes to be copied
     */
    public void copyFrom(byte[] buffer, int start, int length) {
        ensureCapacity(length);
        if (start >= buffer.length) {
            throw new IndexOutOfBoundsException();
        }
        if (start + length > buffer.length) {
            throw new IndexOutOfBoundsException();
        }
        System.arraycopy(buffer, start, bytes, 0, length);
        this.length = length;
    }

    /**
     * Copies the entire contents of the given byte buffer.
     * @param buffer
     */
    public void copyFrom(byte[] buffer) {
        copyFrom(buffer, 0, buffer.length);
    }

    /**
     * Copies the entire contents of the given {@link ByteSequence}
     * @param bs byte sequence to copy from
     */
    public void copyFrom(ByteSequence bs) {
        final int newLength = bs.length();
        ensureCapacity(newLength);
        bs.get(bytes, 0, 0, newLength);
        this.length = newLength;
    }

    /**
     * Copies the entire contents of the given {@link CharSequence}, interpreted
     * as an ASCII string.
     * @param cs char sequence to copy from
     */
    public void copyFrom(CharSequence cs) {
        final int len = cs.length();
        ensureCapacity(len);
        final int newSize = Math.min(this.length, len);
        for (int i = 0; i < newSize; i++) {
            bytes[i] = (byte)cs.charAt(i);
        }
        length = newSize;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte get(int pos) {
        return bytes[pos];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int length() {
        return length;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void get(byte[] buffer, int srcPos, int dstPos, int length) {
        // TODO: add range check
        System.arraycopy(this.bytes, srcPos, buffer, dstPos, length);
    }

    /**
     * Returns this sequence's raw bytes.
     * @return
     */
    public byte[] getBytes() {
        return bytes;
    }

    /**
     * Resets this sequence's length to zero.
     */
    public void clear() {
        length = 0;
    }

    /**
     * Returns this sequence as an ASCII string.
     */
    @Override
    public String toString() {
        return new String(bytes, 0, length, ASCII_CHARSET);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof MutableByteString)) {
            return false;
        }

        final MutableByteString other = (MutableByteString) o;
        if (length != other.length) {
            return false;
        }

        for (int i = 0; i < length; i++) {
            if (bytes[i] != other.bytes[i]) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        for (int i = 0; i < length; i++) {
            hash = 31 * hash + bytes[i];
        }
        return hash;
    }

    private void ensureCapacity(int minCapacity) {
        final int oldCapacity = bytes.length;
        if (minCapacity > oldCapacity) {
            int newCapacity = (oldCapacity * 3) / 2 + 1;
            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            }
            bytes = Arrays.copyOf(bytes, newCapacity);
        }
    }

}
