package org.kftech.prolo;

import java.io.IOException;

/**
 * Base class for all generated protobuf messages and extensions.
 */
public abstract class ProtobufMessageBase {

    /**
     * Allocator for new generated classes or extensions of type {@code T}.
     * @param <T> message or extension type
     */
    public interface Allocator<T extends ProtobufMessageBase> {

        /**
         * Returns a new message instance.
         * @param parent containing message or {@code null} if message is at the root of the object graph
         * @param depth depth of the message within the object graph
         * @param ctx extension context
         * @return new instance of type {@code T}
         */
        T allocate(ProtobufMessage parent, int depth, ExtensionContext ctx);
    }

    /**
     * Clears the state of the message or extension.
     */
    public abstract void clear();

    /**
     * Validates the state of this message or extension.
     *
     * @throws IllegalStateException if the state is not invalid, e.g. required properties
     * have not been set.
     */
    public abstract void validate();

    /**
     * Returns the serialized size of this message in bytes.
     * @return
     */
    public abstract int getSerializedSize();

    /**
     * Writes its serialized form to the given encoder.
     * @param encoder
     * @throws IOException
     */
    public abstract void writeTo(ProtobufEncoder encoder) throws IOException;

    /**
     * Invalidates the cached serialized size (internal method only).
     */
    public abstract void invalidateSerializedSize();

}
