package org.kftech.prolo;

import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * Mutable ASCII string. Pre-allocates a byte array for storage but will grow this if needed.
 */
public final class MutableAsciiString implements CharSequence {

    private static final Charset ASCII_CHARSET;
    static {
        ASCII_CHARSET = Charset.forName("US-ASCII");
    }

    private byte[] bytes;
    private int length;

    /**
     * Creates a mutable string instance with the given capacity.
     * @param capacity capacity in bytes
     */
    public MutableAsciiString(int capacity) {
        this.bytes = new byte[capacity];
        this.length = 0;
    }

    /**
     * Creates a mutable string instance with a capacity of 32 bytes.
     */
    public MutableAsciiString() {
        this(32);
    }

    public void copyFrom(byte[] buffer, int start, int length) {
        ensureCapacity(length);
        if (start >= buffer.length) {
            throw new IndexOutOfBoundsException();
        }
        if (start + length > buffer.length) {
            throw new IndexOutOfBoundsException();
        }
        System.arraycopy(buffer, start, bytes, 0, length);
        this.length = length;
    }

    public void copyFrom(byte[] buffer) {
        copyFrom(buffer, 0, buffer.length);
    }

    public void copyFrom(CharSequence cs) {
        final int len = cs.length();
        ensureCapacity(len);
        for (int i = 0; i < len; i++) {
            bytes[i] = (byte)cs.charAt(i);
        }
        length = len;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public char charAt(int pos) {
        return (char)bytes[pos];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int length() {
        return length;
    }

    /**
     * Not implemented. Always throws {@link UnsupportedOperationException}
     */
    @Override
    public CharSequence subSequence(int start, int end) {
        throw new UnsupportedOperationException("subSequence not supported");
    }

    /**
     * Returns the underlying byte array.
     * @return raw bytes
     */
    public byte[] getBytes() {
        return bytes;
    }

    /**
     * Sets the length of the string to zero.
     */
    public void clear() {
        length = 0;
    }

    /**
     * Returns this string as a new {@code java.lang.String} instance.
     * @return
     */
    @Override
    public String toString() {
        return new String(bytes, 0, length, ASCII_CHARSET);
    }


    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (o instanceof MutableAsciiString) {
            return equals((MutableAsciiString) o);
        } else if (o instanceof CharSequence) {
            return equals((CharSequence) o);
        }

        return false;
    }

    private boolean equals(MutableAsciiString other) {
        if (length != other.length) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (bytes[i] != other.bytes[i]) {
                return false;
            }
        }
        return true;
    }

    private boolean equals(CharSequence cs) {
        if (length != cs.length()) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (charAt(i) != cs.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        for (int i = 0; i < length; i++) {
            hash = 31 * hash + bytes[i];
        }
        return hash;
    }

    private void ensureCapacity(int minCapacity) {
        final int oldCapacity = bytes.length;
        if (minCapacity > oldCapacity) {
            int newCapacity = (oldCapacity * 3) / 2 + 1;
            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            }
            bytes = Arrays.copyOf(bytes, newCapacity);
        }
    }

}
