package org.kftech.prolo;

import java.io.IOException;

/**
 * Base class for all generated message classes.
 */
public abstract class ProtobufMessage extends ProtobufMessageBase {

    /**
     * Registered extensions for this message instance.
     */
    protected ProtobufMessageExtension<?>[] extensions;


    /**
     * Returns a registered extension of the given type.
     * @param clazz extension type class
     * @param <T> extension type
     * @return registered extension or {@code null} if no extension of the given type has been registered
     */
    public <T extends ProtobufMessageExtension<?>> T getExtension(Class<T> clazz) {
        for (int i = 0; i < extensions.length; i++) {
            if (clazz.isAssignableFrom(extensions[i].getClass())) {
                return clazz.cast(extensions[i]);
            }
        }
        return null;
    }

    /**
     * Reads the serialized form of a message using the given decoder
     * @param decoder
     * @throws IOException if deserialization failed
     */
    public abstract void readFrom(ProtobufDecoder decoder) throws IOException;


}
