package org.kftech.prolo;

/**
 * Common interface for all generated enum classes.
 */
public interface ProtobufEnum {

    /**
     * Returns the unique numeric value for this enum instance.
     * @return enum number
     */
    int getNumber();
}
