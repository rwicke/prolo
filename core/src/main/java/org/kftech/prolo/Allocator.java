package org.kftech.prolo;

/**
 * Implementations allocate new instances of a given type.
 * @param <T> type of the instances being allocated
 */
public interface Allocator<T> {

    /**
     * Allocates a new instance of type {@code T}.
     * @return new instance
     */
    T allocate();

}
