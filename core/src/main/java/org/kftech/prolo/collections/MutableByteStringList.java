package org.kftech.prolo.collections;

import org.kftech.prolo.Allocator;
import org.kftech.prolo.MutableByteString;

public final class MutableByteStringList extends ObjectList<MutableByteString> {

    private static final Allocator<MutableByteString> ALLOCATOR = new Allocator<MutableByteString>() {
        @Override
        public MutableByteString allocate() {
            return new MutableByteString(32);
        }
    };

    public MutableByteStringList(int capacity) {
        this(capacity, ALLOCATOR);
    }

    public MutableByteStringList() {
        this(ALLOCATOR);
    }

    public MutableByteStringList(int capacity, Allocator<MutableByteString> mutableByteStringAllocator) {
        super(capacity, mutableByteStringAllocator);
    }

    public MutableByteStringList(Allocator<MutableByteString> mutableByteStringAllocator) {
        super(mutableByteStringAllocator);
    }
}
