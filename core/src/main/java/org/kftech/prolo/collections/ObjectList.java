package org.kftech.prolo.collections;

import org.kftech.prolo.Allocator;

import java.util.Arrays;

/**
 * List of pre-allocated objects. Pre-allocates objects up to its maximum capacity using
 * an
 * @param <T>
 */
public class ObjectList<T> {

    private final Allocator<T> allocator;
    private T[] elements;
    private int size = 0;

    @SuppressWarnings("unchecked")
    public ObjectList(int capacity, Allocator<T> allocator) {
        elements = (T[])new Object[capacity];
        this.allocator = allocator;
        for (int i = 0; i < capacity; i++) {
            elements[i] = allocator.allocate();
        }

    }

    public ObjectList(Allocator<T> allocator) {
        this(16, allocator);
    }

    public T add() {
        ensureCapacity(size + 1);
        return elements[size++];
    }

    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("index " + index + " out of bounds");
        }
        return elements[index];
    }

    public int size() {
        return size;
    }

    public void clear() {
        size = 0;
    }

    private void ensureCapacity(int minCapacity) {
        final int oldCapacity = elements.length;
        if (minCapacity > oldCapacity) {
            int newCapacity = (oldCapacity * 3) / 2 + 1;
            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            }
            elements = Arrays.copyOf(elements, newCapacity);
            for (int i = oldCapacity; i < newCapacity; i++) {
                elements[i] = allocator.allocate();
            }
        }
    }
}
