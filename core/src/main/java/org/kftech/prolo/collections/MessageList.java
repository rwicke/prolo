package org.kftech.prolo.collections;

import org.kftech.prolo.ExtensionContext;
import org.kftech.prolo.ProtobufMessage;
import org.kftech.prolo.ProtobufMessageBase;

import java.util.Arrays;

public final class MessageList<T extends ProtobufMessageBase> {

    private final ProtobufMessageBase.Allocator<T> allocator;
    private Object[] elements;
    private int size = 0;
    private final ProtobufMessage parent;
    private final int depth;
    private final ExtensionContext extensionContext;


    @SuppressWarnings("unchecked")
    public MessageList(int capacity, ProtobufMessageBase.Allocator<T> allocator,
                       ProtobufMessage parent, int depth, ExtensionContext ctx) {
        this.parent = parent;
        this.depth = depth;
        this.extensionContext = ctx;
        elements = new Object[capacity];
        this.allocator = allocator;
        for (int i = 0; i < capacity; i++) {
            elements[i] = allocator.allocate(parent, depth, ctx);
        }

    }

    public MessageList(ProtobufMessageBase.Allocator<T> allocator, ProtobufMessage parent, int depth,
                       ExtensionContext ctx) {
        this(16, allocator, parent, depth, ctx);
    }

    public T add() {
        ensureCapacity(size + 1);
        return (T) elements[size++];
    }

    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("index " + index + " out of bounds");
        }
        return (T) elements[index];
    }

    public int size() {
        return size;
    }

    public void clear() {
        size = 0;
    }

    private void ensureCapacity(int minCapacity) {
        final int oldCapacity = elements.length;
        if (minCapacity > oldCapacity) {
            int newCapacity = (oldCapacity * 3) / 2 + 1;
            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            }
            elements = Arrays.copyOf(elements, newCapacity);
            for (int i = oldCapacity; i < newCapacity; i++) {
                elements[i] = allocator.allocate(parent, depth, extensionContext);
            }
        }
    }
}
