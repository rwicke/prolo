package org.kftech.prolo.collections;

import org.kftech.prolo.Allocator;
import org.kftech.prolo.MutableAsciiString;

public class MutableStringList extends ObjectList<MutableAsciiString> {

    private static final Allocator<MutableAsciiString> ALLOCATOR = new Allocator<MutableAsciiString>() {
        @Override
        public MutableAsciiString allocate() {
            return new MutableAsciiString(32);
        }
    };

    public MutableStringList(int capacity) {
        this(capacity, ALLOCATOR);
    }

    public MutableStringList() {
        this(ALLOCATOR);
    }

    public MutableStringList(int capacity, Allocator<MutableAsciiString> mutableStringAllocator) {
        super(capacity, mutableStringAllocator);
    }

    public MutableStringList(Allocator<MutableAsciiString> mutableStringAllocator) {
        super(mutableStringAllocator);
    }
}
