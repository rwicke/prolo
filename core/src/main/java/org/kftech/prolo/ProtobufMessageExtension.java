package org.kftech.prolo;

import java.io.IOException;

@SuppressWarnings("UnusedDeclaration")
public abstract class ProtobufMessageExtension<T extends ProtobufMessage> extends ProtobufMessageBase {

    /**
     * Attempts to read a message extension.
     * @param tag field number under which the extension has been registered
     * @param decoder decoder instance to use for deserialization
     * @return {@code true} if the extension has been read; {@code false} otherwise
     * @throws IOException
     */
    public abstract boolean readFrom(int tag, ProtobufDecoder decoder) throws IOException;

}
