package org.kftech.prolo;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Various utility methods.
 */
class Utils {
    private Utils() {

    }

    /** Calls Class.getMethod and throws a RuntimeException if it fails. */
    @SuppressWarnings("unchecked")
    public static Method getMethodOrDie(
            final Class clazz, final String name, final Class... params) {
        try {
            return clazz.getMethod(name, params);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(
                    "Generated message class \"" + clazz.getName() +
                            "\" missing method \"" + name + "\".", e);
        }
    }

    /** Calls invoke and throws a RuntimeException if it fails. */
    public static Object invokeOrDie(
            final Method method, final Object object, final Object... params) {
        try {
            return method.invoke(object, params);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(
                    "Couldn't use Java reflection to implement protocol message " +
                            "reflection.", e);
        } catch (InvocationTargetException e) {
            final Throwable cause = e.getCause();
            if (cause instanceof RuntimeException) {
                throw (RuntimeException) cause;
            } else if (cause instanceof Error) {
                throw (Error) cause;
            } else {
                throw new RuntimeException(
                        "Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

}
