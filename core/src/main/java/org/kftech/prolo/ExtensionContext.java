package org.kftech.prolo;

import java.lang.reflect.Method;
import java.util.*;

/**
 * A registry for message extensions.
 */
public final class ExtensionContext {

    private final Map<Class<?>, List<ProtobufMessageBase.Allocator<ProtobufMessageExtension<?>>>> extensionAllocators =
            new HashMap<Class<?>, List<ProtobufMessageBase.Allocator<ProtobufMessageExtension<?>>>>(16);

    /**
     * Registers a message extension for a protobuf message. More than one extension can be registered
     * for any given message type.
     * @param msgClass class of the protobuf message
     * @param extClass class of the protobuf message's extension
     * @param <T> type of the protobuf message class
     */
    public <T extends ProtobufMessage> void registerExtension(Class<T> msgClass, Class<? extends ProtobufMessageExtension<T>> extClass) {
        List<ProtobufMessageBase.Allocator<ProtobufMessageExtension<?>>> allocators = extensionAllocators.get(msgClass);
        if (allocators == null) {
            allocators = new ArrayList<ProtobufMessageBase.Allocator<ProtobufMessageExtension<?>>>(4);
            extensionAllocators.put(msgClass, allocators);
        }
        ProtobufMessageBase.Allocator<ProtobufMessageExtension<?>> allocator = getAllocator(extClass);
        allocators.add(allocator);
    }

    /**
     * Returns allocators for all extensions that have been registered for a protobuf message class.
     * @param msgClass
     * @param <T> type of the protobuf message class
     * @return allocators for registered extensions or an zero-length array if no extensions have
     * been registered.
     */
    @SuppressWarnings("unchecked")
    public <T extends ProtobufMessage> ProtobufMessageBase.Allocator<ProtobufMessageExtension<T>>[] getExtensionAllocators(Class<T> msgClass) {
        List<ProtobufMessageBase.Allocator<ProtobufMessageExtension<?>>> allocators = extensionAllocators.get(msgClass);
        if (allocators == null) {
            allocators = Collections.emptyList();
        }
        return (ProtobufMessageBase.Allocator<ProtobufMessageExtension<T>>[]) allocators.toArray(
                new ProtobufMessageBase.Allocator<?>[allocators.size()]);
    }

    @SuppressWarnings("unchecked")
    private ProtobufMessageBase.Allocator<ProtobufMessageExtension<?>> getAllocator(Class<?> clazz) {
        Method getAllocator = Utils.getMethodOrDie(clazz, "getAllocator");
        return (ProtobufMessageBase.Allocator<ProtobufMessageExtension<?>>) Utils.invokeOrDie(getAllocator, clazz);
    }


}
