package org.kftech.prolo;

/**
 * An indexed sequence of bytes.
 */
public interface ByteSequence {

    /**
     * Gets the byte at the given index
     * @param index sequence index
     * @return byte at {@code index}
     * @throws IndexOutOfBoundsException if {@code index} is less than zero or greater or
     * the sequence length
     */
    byte get(int index);

    /**
     * Copies this sequence's bytes into the given buffer (bulk get).
     *
     * @param buffer target buffer
     * @param srcPos index of the first byte in this sequence to be copied from
     * @param dstPos index of the first byte in {@code buffer} to be copied to
     * @param length number of bytes to copy
     */
    void get(byte[] buffer, int srcPos, int dstPos, int length);

    /**
     * Returns the length of the byte sequence
     * @return length
     */
    int length();

}
