primitiveCollection(primitiveType,typeName) ::= <<

package org.kftech.prolo.collections;

import java.util.Arrays;

public final class <typeName>List {

    private <primitiveType>[] elements;
    private int size = 0;

    public <typeName>List(int capacity) {
        elements = new <primitiveType>[capacity];
    }

    public <typeName>List() {
        this(16);
    }

    public void add(<primitiveType> elem) {
        ensureCapacity(size + 1);
        elements[size++] = elem;
    }

    public <primitiveType> get(int index) {
        if (index \< 0 || index \>= size) {
            throw new IndexOutOfBoundsException("index " + index + " out of bounds");
        }
        return elements[index];
    }

    public int size() {
        return size;
    }

    public void clear() {
        size = 0;
    }

    private void ensureCapacity(int minCapacity) {
        final int oldCapacity = elements.length;
        if (minCapacity \> oldCapacity) {
            int newCapacity = (oldCapacity * 3) / 2 + 1;
            if (newCapacity \< minCapacity) {
                newCapacity = minCapacity;
            }
            elements = Arrays.copyOf(elements, newCapacity);
        }
    }
}

>>